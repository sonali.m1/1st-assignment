// Let's get some practice writing a few objects for a new group of interns at a small business.

// ==== Challenge 1: Writing Objects ====
// HR needs some information on the new interns put into a database.  Given an id, email, first name, and gender. Create an object for each person in the company list:

// let id=(mmelloy0@psu.edu,Mitzi,F)
// let email=(mmelloy0@psu.edu,Mitzi,F)
// let firstName=(mmelloy)
// let gender=(Male)
// 1,mmelloy0@psu.edu,Mitzi,F
// 2,kdiben1@tinypic.com,Kennan,M
// 3,kmummery2@wikimedia.org,Keven,M
// 4,gmartinson3@illinois.edu,Gannie,M
// 5,adaine5@samsung.com,Antonietta,F

// Example format of an intern object: 1,examples@you.edu,Example,F
const person1 = {
  id: 0,
  name: "Mitzi",
  email: "mmelloy0@psu.edu",
  gender: "F",
};
const person2 = {
  id: 2,
  name: "Kennan",
  email: "kdiben1@tinypic.com",
  gender: "M",
};
// 3,kmummery2@wikimedia.org,Keven,M
const person3 = {
  id: 3,
  name: "Keven",
  email: "kmummery2@wikimedia.org",
  gender: "M",
};
// 4,gmartinson3@illinois.edu,Gannie,M
const person4 = {
  id: 4,
  name: "Gannie",
  email: "gmartinson3@illinois.edu",
  gender: "M",
};
// 5,adaine5@samsung.com,Antonietta,F
const person5 = {
  id: 5,
  name: "Antonietta",
  email: "adaine5@samsung.com",
  gender: "F",
};

// Write your intern objects here:

// ==== Challenge 2: Reading Object Data ====
// Once your objects are created, log out the following requests from HR into the console:

// Mitzi's name
console.log(person1.name);

// Kennan's ID
console.log(person2.id);

// Keven's email
console.log(person3.email);

// Gannie's name
console.log(person4.name);

// Antonietta's Gender
console.log(person5.gender);

// ==== Challenge 3: Object Methods ====
// Give Kennan the ability to say "Hello, my name is Kennan!" Use the console.log provided as a hint.
// console.log(kennan.speak());
let person = {
  firstName: "Kennan",
};
person.name = function () {
  return this.firstName + " " + Hello;
};
console.log("Hello, my name is " + " " + person.firstName);

// Antonietta loves math, give her the ability to multiply two numbers together and return the product. Use the console.log provided as a hint.
//console.log(antonietta.multiplyNums(3,4));
// MULTIPLY FUNCTION.
const multiply = (num1, num2) => {
  return num1 * num2;
};
let resulMultiply = multiply(4, 5);
console.log(resulMultiply);

// === Great work! === Head over to the the arrays.js file or take a look at the stretch challenge

// ==== Stretch Challenge: Nested Objects and the this keyword ====

// 1. Create a parent object with properties for name and age.  Make the name Susan and the age 70.
// 2. Nest a child object in the parent object with name and age as well.  The name will be George and the age will be 50.
// 3. Nest a grandchild object in the child object with properties for name and age.  The name will be Sam and the age will be 30
// 4. Give each of the objects the ability to speak their names using the this keyword.
var parent = {
  firstName: "Susan",
  age: 70,
  speak: function () {
    console.log(`Hello this is ${this.firstName}`);
  },

  child: {
    firstName: "George",
    age: 50,
    speak: function () {
      console.log(`Hello this is ${this.firstName}`);
    },

    grandchild: {
      firstName: "Sam",
      age: 30,
      speak: function () {
        console.log(`Hello this is ${this.firstName}`);
      },
    },
  },
};

// Log the parent object's name
console.log(parent.firstName);

// Log the child's age
console.log(parent.child.age);

// Log the name and age of the grandchild
console.log(parent.child.grandchild.firstName);
console.log(parent.child.grandchild.age);

// Have the parent speak;
console.log(parent.speak());

// Have the child speak
console.log(parent.child.speak());

// Have the grandchild speak
console.log(parent.child.grandchild.speak());
