// Take the commented ES5 syntax and convert it to ES6 arrow Syntax

// let myFunction = function () {};

// let anotherFunction = function (param) {
//   return param;
// };
// let anotherFunction = function()
function detail(fname, lname) {
  console.log("Hello" + " " + fname + " " + lname);
}

detail("Sonali", "Mishra");

// let add = function (param1, param2) {
//   return param1 + param2;
// };
// add(1,2);

// let subtract = function (param1, param2) {
//   return param1 - param2;
// };
// subtract(1,2);

// exampleArray = [1,2,3,4];
// const triple = exampleArray.map(function (num) {
//   return num * 3;
// });
// console.log(triple);

// let hello = (fname, lname) => fname + lname;
// console.log(hello(fname + " " + lname));

// detail("Sonali", "Mishra");
// detail("Sonali", "Mishra");
// detail("Sonali", "Mishra");
